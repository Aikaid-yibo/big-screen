# 智慧城市大屏可视化

#### 描述

智慧城市大屏可视化，核心展示了地图、胶囊柱图、进度条、Echarts图标、滚动轮播图、水位图等等相关可视化模块。

#### 技术栈

Vue2 + Datav + Echarts

#### 项目亮点

1.  <span style="color: red">封装了“胶囊柱图”组件</span>
2.  <span style="color:red">封装了“Echarts柱状图”组件</span>
3.  <span style="color: red">使用`WebSocket`实现Echarts图表动态数据展示</span>

#### 大屏可视化预览图

![image-20230321213953932](README.assets/image-20230321213953932.png)

#### 克隆预览

- 安装依赖：

  ```
  pnpm i
  或
  npm i
  ```

- 启动项目：

  ```
  pnpm dev
  或
  npm run dev
  ```

#### 开源协议

MIT
